# drm-misc Commit Access

Hi!

I'd like to get commit access to the `drm-misc` repository.

## Motivation

<!-- What do you need commit access for? -->

## History

<!--
We require that committers that committers have some contribution experience
before getting commit access rights.

See the requirements here: https://drm.pages.freedesktop.org/maintainer-tools/commit-access.html#drm-misc

Please provide a list of ~10 non-trivial commits that were already merged in
`drivers/gpu/drm`.
-->

/label ~commit-rights
/assign @mlankhorst @mripard @tzimmermann
/cc @airlied @daniels @sima
