# DRM-Misc Kernel Repository 

This kernel tree handles all drivers but the "big" (amd, nvidia, intel) ones,
with a few exceptions, and most of the common infrastructure work.

This branch is there to hold our templates and Gitlab configuration, chances
are you want to look at either the
[drm-misc-next](https://gitlab.freedesktop.org/drm/misc/kernel/-/tree/drm-misc-next)
or
[drm-misc-fixes](https://gitlab.freedesktop.org/drm/misc/kernel/-/tree/drm-misc-fixes)
branches.
